ibus-skk (1.4.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.4.3.
  * debian: Apply "wrap-and-sort -abst".
  * debian/control:
    + Remove unnecessary dh-autoreconf dpkg-dev build-dep.
    + Remove Daiki Ueno from uploaders list. (Closes: #903252).
    + Bump Standards-Version to 4.2.1.
    + Update homepage field and use secure uri.
  * debian/rules: Modernize and enable full hardening.
  * debian/patches: Drop merged patches and bump valac version requirement
    to 0.36+.

 -- Boyuan Yang <byang@debian.org>  Thu, 27 Sep 2018 10:09:35 -0400

ibus-skk (1.4.2-2) unstable; urgency=medium

  * Update VCS to new salsa service in compliance to the Debian
    Policy 5.6.26 (version 4.1.4).
  * Migrate from alioth to lists.debian.org for the maintainer address.
    Closes: #899816
  * Bump standards version to 4.1.4 and compat to 11.
  * Format with wrap-and-sort.

 -- Osamu Aoki <osamu@debian.org>  Sun, 08 Jul 2018 09:13:23 +0900

ibus-skk (1.4.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add Build-with-vala-0.36.patch to fix build with vala 0.36
    (Closes: #871186)

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 07 Sep 2017 16:10:29 -0400

ibus-skk (1.4.2-1) unstable; urgency=medium

  * New upstream release to use libgee-0.8-dev.  Closes: #753867

 -- Osamu Aoki <osamu@debian.org>  Tue, 21 Oct 2014 21:13:57 +0900

ibus-skk (1.4.1-3) unstable; urgency=low

  * Fix libexecdir. Closes: #712582

 -- Osamu Aoki <osamu@debian.org>  Wed, 19 Jun 2013 22:37:03 +0900

ibus-skk (1.4.1-2) unstable; urgency=low

  * Include more docs in binary package  (Closes: #686472)

 -- Daiki Ueno <ueno@unixuser.org>  Tue, 11 Sep 2012 04:08:52 +0900

ibus-skk (1.4.1-1) unstable; urgency=low

  * New upstream release
  * Update Standards-Version to 3.9.3

 -- Daiki Ueno <ueno@unixuser.org>  Sat, 07 Apr 2012 18:44:26 +0900

ibus-skk (1.4.0-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Thu, 26 Jan 2012 14:59:21 +0900

ibus-skk (1.3.99.20120111-1) unstable; urgency=low

  * New upstream snapshot.
  * Drop python dependency.
  * Build against libskk (>= 0.0.8).
  * Update copyright.

 -- Daiki Ueno <ueno@unixuser.org>  Fri, 13 Jan 2012 11:53:33 +0900

ibus-skk (1.3.9-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Thu, 01 Dec 2011 21:59:30 +0900

ibus-skk (1.3.7-2) unstable; urgency=low

  [ Osamu Aoki ]
  * Follow newer packageing style (closes: #632442)

 -- Daiki Ueno <ueno@unixuser.org>  Sun, 03 Jul 2011 22:30:04 +0900

ibus-skk (1.3.7-1) unstable; urgency=low

  * Add myself to Uploaders.
  * Add VCS fields to control.
  * Bump Standards-Version to 3.9.2.
  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Sat, 02 Jul 2011 13:15:32 +0900

ibus-skk (1.3.6-1) unstable; urgency=low

  * New upstream release.
  * Change Maintainer: to IME Packaging Team.

 -- Daiki Ueno <ueno@unixuser.org>  Fri, 13 May 2011 15:25:17 +0900

ibus-skk (1.3.5-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Sat, 18 Dec 2010 18:22:05 +0900

ibus-skk (1.3.4-1) unstable; urgency=low

  * New upstream release (Closes: #605173)

 -- Daiki Ueno <ueno@unixuser.org>  Tue, 30 Nov 2010 09:51:25 +0900

ibus-skk (1.3.3-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Mon, 08 Nov 2010 20:32:16 +0900

ibus-skk (1.3.2-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Tue, 12 Oct 2010 20:36:02 +0900

ibus-skk (1.3.1-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Mon, 11 Oct 2010 13:02:32 +0900

ibus-skk (1.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Daiki Ueno <ueno@unixuser.org>  Sun, 29 Aug 2010 14:25:59 +0900

ibus-skk (0.0.10-1) unstable; urgency=low

  * New upstream release (Closes: #590188, #590191, #591052)
  * Bump Standards-Version to 3.9.1.

 -- Daiki Ueno <ueno@unixuser.org>  Tue, 03 Aug 2010 06:06:58 +0900

ibus-skk (0.0.9-1) unstable; urgency=low

  * New upstream release (Closes: #583784)
  * Add debian/source/format.

 -- Daiki Ueno <ueno@unixuser.org>  Tue, 29 Jun 2010 05:58:19 +0900

ibus-skk (0.0.8-1) unstable; urgency=low

  * New upstream release.
  * Fix debian/watch entry.

 -- Daiki Ueno <ueno@unixuser.org>  Sun, 23 May 2010 16:57:40 +0900

ibus-skk (0.0.7-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.8.4.

 -- Daiki Ueno <ueno@unixuser.org>  Sat, 08 May 2010 14:18:03 +0900

ibus-skk (0.0.6-1) unstable; urgency=low

  * New upstream release (Closes: #580231)

 -- Daiki Ueno <ueno@unixuser.org>  Sat, 08 May 2010 08:20:40 +0900

ibus-skk (0.0.4-1) unstable; urgency=low

  * New upstream release.
  * Fix debian/watch entry to extract the cloud.github.com URL from the
    download page.

 -- Daiki Ueno <ueno@unixuser.org>  Sun, 24 Jan 2010 10:43:15 +0900

ibus-skk (0.0.2-1) unstable; urgency=low

  * Initial release (Closes: #562137)

 -- Daiki Ueno <ueno@unixuser.org>  Wed, 23 Dec 2009 11:46:22 +0900
